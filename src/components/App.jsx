import React, { lazy, Suspense } from "react";

import { createTheme, ThemeProvider } from '@mui/material/styles';

const LoginApp = lazy(() => import("LoginApp/app"));

const defaultTheme = createTheme();

const App = () => {

  return (
    <ThemeProvider theme={defaultTheme}>
      <div>
        <Suspense fallback={<span>Loading...</span>}>
          <LoginApp />
        </Suspense>
      </div>
    </ThemeProvider>
  );
};

export default App;