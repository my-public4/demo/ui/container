import React, { lazy, Suspense } from "react";

import { createTheme, ThemeProvider } from "@mui/material/styles";
import {
	Grid,
	Container,
	Typography,
	AppBar,
	IconButton,
	Box,
	Avatar,
	Toolbar,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";

import AccountBalanceIcon from "@mui/icons-material/AccountBalance";

import {useParams} from 'react-router-dom';


import TransactionApp from "TransactionApp/app";

const defaultTheme = createTheme();

const Transaction = () => {

  const { id } = useParams();

  const handleOpenUserMenu = () => {

  }

  const handleOpenNavMenu = () => {

  }

	return (
		<ThemeProvider theme={defaultTheme}>
			<AppBar position="static">
				<Container maxWidth="xl">
					<Toolbar disableGutters>
						<AccountBalanceIcon
							sx={{ display: { xs: "none", md: "flex" }, mr: 1 }}
						/>
						<Typography
							variant="h6"
							noWrap
							component="a"
							href="/"
							sx={{
								mr: 2,
								display: { xs: "none", md: "flex" },
								fontFamily: "monospace",
								fontWeight: 700,
								letterSpacing: ".3rem",
								color: "inherit",
								textDecoration: "none",
							}}
						>
							MicroBank
						</Typography>

						<Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
							<IconButton
								size="large"
								aria-label="account of current user"
								aria-controls="menu-appbar"
								aria-haspopup="true"
								onClick={handleOpenNavMenu}
								color="inherit"
							>
								<MenuIcon />
							</IconButton>
							
						</Box>
						<AccountBalanceIcon
							sx={{ display: { xs: "flex", md: "none" }, mr: 1 }}
						/>
						<Typography
							variant="h5"
							noWrap
							component="a"
							href=""
							sx={{
								mr: 2,
								display: { xs: "flex", md: "none" },
								flexGrow: 1,
								fontFamily: "monospace",
								fontWeight: 700,
								letterSpacing: ".3rem",
								color: "inherit",
								textDecoration: "none",
							}}
						>
							LOGO
						</Typography>
						<Box
							sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}
						></Box>

						<Box sx={{ flexGrow: 0 }}>
							<IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
								<Avatar alt="David Dang" src="" />
							</IconButton>
						</Box>
					</Toolbar>
				</Container>
			</AppBar>
			<Container maxWidth="xl">
				<Grid p={6}>
					<Suspense fallback={<span>Loading...</span>}>
						<TransactionApp id={id} />
					</Suspense>
				</Grid>
			</Container>
		</ThemeProvider>
	);
};

export default Transaction;
