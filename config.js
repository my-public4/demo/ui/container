module.exports = {
  LOGIN_APP_URL: process.env.LOGIN_APP_URL,
  DASHBOARD_APP_URL: process.env.DASHBOARD_APP_URL,
  TRANSACTION_APP_URL: process.env.TRANSACTION_APP_URL,
  API_HOST: process.env.API_HOST
}